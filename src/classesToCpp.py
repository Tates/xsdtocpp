#!/bin/python3

import os
from CPPEnum import (EnumValues, CppEnum)
from CPPClass import (FieldType, CppClass)
import XSDReader


def createAndFillFile(path, content):
    generatedFileHPP = path
    dir = os.path.dirname(path)
    if not os.path.exists(dir):
        os.makedirs(dir, exist_ok=True)
    with open(generatedFileHPP, 'w') as f:
        f.write(content)


def generateClasses(cppCs, cppEs):
    for cppC in cppCs:
        (cppContent, hppContent) = cppC.generateContent()
        createAndFillFile(cppC.cppPath(), cppContent)
        createAndFillFile(cppC.hppPath(), hppContent)
        (classParserCPP, classParserHPP) = cppC.generateParser()
        createAndFillFile(cppC.cppParsePath(), classParserCPP)
        createAndFillFile(cppC.hppParsePath(), classParserHPP)
    for cppe in cppEs:
        (enumCPP, enumHPP) = cppe.generateContent()
        createAndFillFile(cppe.cppPath(), enumCPP)
        createAndFillFile(cppe.hppPath(), enumHPP)
        (enumParserCPP, enumParserHPP) = cppe.generateParser()
        createAndFillFile(cppe.cppParsePath(), enumParserCPP)
        createAndFillFile(cppe.hppParsePath(), enumParserHPP)

def convertXsdToObj(xsdPath):
    return XSDReader.getClassesFromXSD(xsdPath)


def __MAIN__():
    (cppCs, cppes) = convertXsdToObj('')
    generateClasses(cppCs, cppes)


__MAIN__()
