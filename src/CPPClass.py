#!/bin/python3

class FieldType:
    def __init__(self, typeStr, name, isPrimitive=False) -> None:
        self.typeStr = typeStr
        self.name = name
        self.isPrimitive = isPrimitive

    def getSetHpp(self):
        template = '''    const {type}& {name}() const;
    void set_{name}(const {type}&);
'''
        if (self.isPrimitive):
            template = '''    {type} {name}() const;
    void set_{name}({type});
'''
        d = {}
        d['name'] = self.name
        d['type'] = self.typeStr
        return template.format(**d)

    def getSetCpp(self) -> str:
        template = '''const {typeName}& {{className}}::{name}() const
{{{{
    return m_{name};
}}}}

void {{className}}::set_{name}(const {typeName}& value)
{{{{
    m_{name} = value;
}}}}
'''
        if (self.isPrimitive):
            template = '''{typeName} {{className}}::{name}() const
{{{{
    return m_{name};
}}}}

void {{className}}::set_{name}({typeName} value)
{{{{
    m_{name} = value;
}}}}
'''
        d = {}
        d['name'] = self.name
        d['typeName'] = self.typeStr
        return template.format(**d)

    def fromJson(self):
        template = '    value.set_{name}(j.get<{typName}>());\n'
        return template.format(name=self.name, typName=self.typeStr)

    def toJson(self):
        template = '    j["{name}"] = value.{name};\n'
        return template.format(name=self.name)


class CppClass:
    def __init__(self, name='', fields: list[FieldType] = [], includes=[]) -> None:
        self.name = name
        self.fields = fields
        self.includes = includes

    def cppPath(self) -> str:
        return 'output/src/{name}.cpp'.format(name=self.name)

    def hppPath(self) -> str:
        return 'output/include/{name}.hpp'.format(name=self.name)

    def cppParsePath(self) -> str:
        return 'output/src/parser/{name}Parser.cpp'.format(name=self.name)

    def hppParsePath(self) -> str:
        return 'output/include/parser/{name}Parser.hpp'.format(name=self.name)

    def fieldsStr(self) -> str:
        fieldsStr = ''
        for field in self.fields:
            d = {}
            d['fieldName'] = field.name
            d['fieldType'] = field.typeStr
            fieldsStr += '    {fieldType} m_{fieldName};\n'.format(**d)
        return fieldsStr

    def generateContentHpp(self):
        templateHpp = '''#pragma once

class {className} {{
public:
    {className}();
{getSet}
private:
{fields}
}};
'''
        d = {}
        d['className'] = self.name
        d['getSet'] = self.getSetHpp()
        fieldsStr = ''
        for field in self.fields:
            fieldsStr += '    ' + field.typeStr + ' m_' + field.name + ';\n'
        d['fields'] = fieldsStr
        return templateHpp.format(**d)

    def getSetHpp(self):
        getSetStr = ''
        for field in self.fields:
            getSetStr += field.getSetHpp()
        return getSetStr

    def getSetCpp(self):
        getSetStr = ''
        for field in self.fields:
            getStr = field.getSetCpp()
            getSetStr += getStr.format(className=self.name)
        return getSetStr

    def generateContentCpp(self):
        templateCPP = '''#include "{hppInclude}"

{className}::{className}({args}){fields}
{{}}

{getSet}
'''
        d = {}
        d['className'] = self.name
        d['args'] = ''
        d['fields'] = ''
        d['hppInclude'] = self.name + '.hpp'
        d['getSet'] = self.getSetCpp()
        return templateCPP.format(**d)

    def generateContent(self):
        return (self.generateContentCpp(), self.generateContentHpp())

    def generateParser(self):
        return (self.generateParserCpp(), self.generateParserHpp())

    def generateParserHpp(self):
        templateHpp = '''#pragma once
{includes}
#include <nlohmann/json.hpp>

template <>
struct nlohmann::adl_serializer<{className}> {{
    void to_json(nlohmann::json& j, const {className}& value);
    void from_json(const nlohmann::json& j, {className}& value);
}};
'''
        d = {}
        d['includes'] = '\n#include "../{name}.hpp"\n'.format(name=self.name)
        d['className'] = self.name

        return templateHpp.format(**d)

    def generateParserCpp(self):
        templateCpp = '''{includes}

void nlohmann::adl_serializer<{enumName}>::to_json(nlohmann::json& j, const {enumName}& value)
{{
{toJson}}}

void nlohmann::adl_serializer<{enumName}>::from_json(const nlohmann::json& j, {enumName}& value)
{{
{fromJson}}}
'''
        d = {}
        d['enumName'] = self.name
        d['includes'] = '#include "parser/{className}Parser.hpp"'.format(
            className=self.name)
        d['fromJson'] = self.fromJson()
        d['toJson'] = self.toJson()
        return templateCpp.format(**d)

    def fromJson(self):
        fromJsonStr = ''
        for field in self.fields:
            fromJsonStr += field.fromJson()
        return fromJsonStr

    def toJson(self):
        toJsonStr = ''
        for field in self.fields:
            toJsonStr += field.toJson()
        return toJsonStr
