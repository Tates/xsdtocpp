#!/bin/python3

class EnumValues:
    def __init__(self, name, value) -> None:
        self.name = name
        self.value = value

    def valueDefine(self) -> str:
        return '    {name}  = {value},\n'.format(name=self.name, value=self.value)

    def valueToString(self) -> str:
        return '    {{{{ {{enumName}}::{name}, "{name}" }}}},\n'.format(name=self.name)


class CppEnum:
    def __init__(self, name='Foo', values: list[EnumValues] = []) -> None:
        self.name = name
        self.values = values

    def generateContent(self):
        return (self.generateContentCpp(), self.generateContentHpp())

    def generateContentHpp(self):
        enumTemplateHPP = '''#pragma once

#include <string>

enum class {enumName} {{
{values}}};

std::string {enumName}ToString({enumName} val);
{enumName} {enumName}FromString(const std::string& str);
'''
        d = {}
        d['enumName'] = self.name
        valuesStr = ''
        for val in self.values:
            valuesStr += val.valueDefine()
        d['values'] = valuesStr
        enumContent = enumTemplateHPP.format(**d)
        return enumContent

    def generateContentCpp(self):
        template = '''{include}
namespace {{
const std::map<std::string, {name}> c_defines {{
{toStrs}}};
}}

std::string {name}ToString({name} val)
{{
    for (auto it = c_defines.begin(); it != c_defines.end(); ++it) {{
        if (it.second == val) {{
            return it.first;
        }}
    }}
    return "[INVALID VALUE OF {name}]";
}}

{name} {name}FromString(const std::string& str)
{{
    if (!c_defines.contains(str)) {{
        return ({name})0;
    }}
    return c_defines.find(str);
}}
'''
        d = {}
        d['include'] = '#include "{inc}.hpp"\n\n#include <map>\n'.format(
            inc=self.name)
        d['name'] = self.name
        toStrs = ''
        for v in self.values:
            toStrs += v.valueToString().format(enumName=self.name)
        d['toStrs'] = toStrs
        return template.format(**d)

    def generateParserHpp(self):
        templateHpp = '''#pragma once
{includes}
#include <nlohmann/json.hpp>

template <>
struct nlohmann::adl_serializer<{enumName}> {{
    void to_json(nlohmann::json& j, const {enumName}& value);
    void from_json(const nlohmann::json& j, {enumName}& value);
}};
'''
        d = {}
        d['includes'] = '\n#include "../{name}.hpp"\n'.format(name=self.name)
        d['enumName'] = self.name

        return templateHpp.format(**d)

    def generateParserCpp(self):
        templateCpp = '''{includes}

void nlohmann::adl_serializer<{enumName}>::to_json(nlohmann::json& j, const {enumName}& value)
{{
{toJson}}}

void nlohmann::adl_serializer<{enumName}>::from_json(const nlohmann::json& j, {enumName}& value)
{{
{fromJson}}}
'''
        d = {}
        d['enumName'] = self.name
        d['includes'] = '#include "parser/{enumName}Parser.hpp"'.format(
            enumName=self.name)
        d['fromJson'] = self.fromJson()
        d['toJson'] = self.toJson()
        return templateCpp.format(**d)

    def generateParser(self):
        return (self.generateParserCpp(), self.generateParserHpp())

    def fromJson(self):
        template = '    value = {valName}FromString(j.get<std::string>());\n'
        return template.format(valName=self.name)

    def toJson(self):
        template = '    j = {valName}ToString(value);\n'
        return template.format(valName=self.name)

    def cppPath(self) -> str:
        return 'output/src/{name}.cpp'.format(name=self.name)

    def hppPath(self) -> str:
        return 'output/include/{name}.hpp'.format(name=self.name)

    def cppParsePath(self) -> str:
        return 'output/src/parser/{name}Parser.cpp'.format(name=self.name)

    def hppParsePath(self) -> str:
        return 'output/include/parser/{name}Parser.hpp'.format(name=self.name)
