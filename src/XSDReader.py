#!/bin/python3

from CPPClass import (FieldType, CppClass)
from CPPEnum import (EnumValues, CppEnum)


def getClassesFromXSD(xsdPath):
    fields: list[FieldType] = [
        FieldType('bool', 'bar', True)
    ]

    className = 'Foo'
    classes: list[CppClass] = [
        CppClass(className, fields)
    ]
    enumName = 'Fruit'
    values: list[EnumValues] = [
        EnumValues('Orange', 0),
        EnumValues('Banana', 1),
        EnumValues('Clementine', 2)
    ]
    enums: list[CppEnum] = [
        CppEnum(enumName, values)
    ]
    return (classes, enums)
